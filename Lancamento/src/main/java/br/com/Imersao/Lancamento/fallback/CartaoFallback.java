package br.com.Imersao.Lancamento.fallback;

import java.util.Optional;

import org.springframework.stereotype.Component;

import br.com.Imersao.Lancamento.dtos.Cartao;
import br.com.Imersao.Lancamento.repositories.CartaoClient;

@Component
public class CartaoFallback implements CartaoClient {

	@Override
	public Optional<Cartao> buscar(String numCartao) {
		Cartao cartao = new Cartao();
		cartao.setStatus("Pendente");
		cartao.setAtivo(true);
		cartao.setNumCartao(numCartao);

		return Optional.of(cartao);

	}

}
