package br.com.Imersao.Lancamento.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.Lancamento.models.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Integer>{
  Iterable<Lancamento> findAllBynumCartao(String numCartao);
}
