package br.com.Imersao.Cartao.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.Cartao.models.Cartao;
import br.com.Imersao.Cartao.services.CartaoService;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
  @Autowired
  private CartaoService cartaoService;
  
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Cartao criar(@Valid @RequestBody Cartao cartao) {
    return cartaoService.criar(cartao);
  }
  
  @GetMapping("/{numCartao}")
  public Optional<Cartao> buscar(@PathVariable String numCartao) {
    return cartaoService.buscar(numCartao);
  }
  
  @PatchMapping("/{numero}/ativar")
  public void ativar(@PathVariable String numero) {  
    Optional<Cartao> optional = cartaoService.buscar(numero);
    
    if(!optional.isPresent()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    
    cartaoService.ativar(numero);
  }
}
