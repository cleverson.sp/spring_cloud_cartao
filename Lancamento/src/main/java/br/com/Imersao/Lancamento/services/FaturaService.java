package br.com.Imersao.Lancamento.services;

import java.math.BigDecimal;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.Lancamento.models.Fatura;
import br.com.Imersao.Lancamento.models.Lancamento;

@Service
public class FaturaService {

	@Autowired
	private LancamentoService lancamentoService;
	
	public Fatura buscar(String numero) {
		Iterable<Lancamento> lancamentos = lancamentoService.buscarTodasPorNumeroDoCartao(numero);
		
		Fatura fatura = new Fatura();
		fatura.setLancamentos(lancamentos);
		
		Iterator<Lancamento> iterator = lancamentos.iterator();
		BigDecimal soma = new BigDecimal(0);
		while(iterator.hasNext()) {
			soma = soma.add(iterator.next().getValor());
		}
		
		fatura.setTotal(soma);
		return fatura;
	}
	
}
