package br.com.Imersao.Cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.Cartao.models.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String>{}
