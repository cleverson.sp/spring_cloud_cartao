package br.com.Imersao.Lancamento.services;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.Lancamento.dtos.Cartao;
import br.com.Imersao.Lancamento.exceptions.ValidacaoException;
import br.com.Imersao.Lancamento.models.Lancamento;
import br.com.Imersao.Lancamento.repositories.CartaoClient;
import br.com.Imersao.Lancamento.repositories.LancamentoRepository;

@Service
public class LancamentoService {
  
  @Autowired
  private LancamentoRepository lancamentoRepository;
  
  @Autowired
  private CartaoClient cartaoClient;

  public Lancamento criar(Lancamento lancamento) {  
	
    Optional<Cartao> optional = cartaoClient.buscar(lancamento.getNumCartao());
    
    if(optional.get().getStatus().equals("Pendente")) {
    	lancamento.setNumCartao(optional.get().getNumCartao());
    	lancamento.setStatus(optional.get().getStatus());
    }
    
    if(!optional.isPresent()) {
      throw new ValidacaoException("cartao", "Cartão não encontrado");
    }

    if(!optional.get().isAtivo()) {
        throw new ValidacaoException("cartao", "Cartão não ativo");
    }
    
    lancamento.setHorario(LocalDateTime.now());
        
    return lancamentoRepository.save(lancamento);
    
  }
  
  public Iterable<Lancamento> buscarTodasPorNumeroDoCartao(String numCartao){
    return lancamentoRepository.findAllBynumCartao(numCartao);
  }
}
