package br.com.Imersao.Cartao.services;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.Cartao.dtos.Cliente;
import br.com.Imersao.Cartao.exceptions.ValidacaoException;
import br.com.Imersao.Cartao.models.Cartao;
import br.com.Imersao.Cartao.repositories.CartaoRepository;
import br.com.Imersao.Cartao.repositories.ClienteClient;
//import br.itau.com.cartoes.exceptions.ValidacaoException;

@Service
public class CartaoService {
  @Autowired
  private CartaoRepository cartaoRepository;
  
  @Autowired
  private ClienteClient clienteClient;
  
  public Cartao criar(Cartao cartao) {
	  
    Optional<Cliente> optional = clienteClient.buscar(cartao.getIdCliente());
    
    if(!optional.isPresent()) {
       throw new ValidacaoException("cliente", "Cliente não encontrado");
    }
    
    cartao.setNumero(gerarNumero());
    cartao.setAtivo(false);
    
    return cartaoRepository.save(cartao);
  }
  
  public Optional<Cartao> buscar(String numCartao){
    return cartaoRepository.findById(numCartao);
  }
  
  private String gerarNumero() {
    Random random = new Random();
    String numero = "";
    
    for(int i = 0; i < 16; i ++) {
       numero += random.nextInt(9);
    }
    
    if(cartaoRepository.findById(numero).isPresent()) {
      return gerarNumero();
    }
    
    return numero;
  }
  
  public void ativar(String numero) {
    Optional<Cartao> cartaoOptional = cartaoRepository.findById(numero);
    
    if(!cartaoOptional.isPresent()) {
      throw new ValidacaoException("numero", "Cartão não encontrado.");
    }
    
    Cartao cartao = cartaoOptional.get();
    
    if(cartao.getAtivo()) {
      throw new ValidacaoException("numero", "Cartão já está ativo.");
    }
    
    cartao.setAtivo(true);
    cartaoRepository.save(cartao);
  }
}
