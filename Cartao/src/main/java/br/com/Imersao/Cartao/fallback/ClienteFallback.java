package br.com.Imersao.Cartao.fallback;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.Cartao.dtos.Cliente;
import br.com.Imersao.Cartao.repositories.ClienteClient;

@Component
public class ClienteFallback implements ClienteClient {

	@Override
	public Optional<Cliente> buscar(int id) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Serviço de Cliente fora");
	}
	
	  

}
