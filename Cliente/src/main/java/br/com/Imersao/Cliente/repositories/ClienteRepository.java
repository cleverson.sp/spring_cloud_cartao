package br.com.Imersao.Cliente.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.Cliente.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
  Optional<Cliente> findByCpf(String cpf);
}
